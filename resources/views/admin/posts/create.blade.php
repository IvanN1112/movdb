<!DOCTYPE HTML>
@extends('layouts.admin');

@section('content')


    @component('admin.includes.title')
        Add Post / Authors    
    @endcomponent


    <form method="POST" action="/admin/posts" enctype="multipart/form-data">
        @csrf

        
        <div class="row">
            <div class="col-sm-4">
                
                <div class="form-group">
                    <label for="file">Movie Pic</label>
                    <div class="">
                        <img src="{{ url('images/no_movie_ph.png') }}" id="profile-img-tag" width="295px" style="margin-bottom: 0.5rem" alt="">
                    </div>
                    <input type="file" name="file" id="profile-img">
                </div>
            </div>

            <div class="col-sm-8">
                    
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" placeholder="Enter your post title">
                    </div>

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Enter your movie name">
                    </div>

                    <div class="form-group">
                        <label for="category_id">Category</label>
                        <select name="category_id" id="category_id" class="form-control">
                            <option disabled selected>select a category</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control" cols="5" rows="5"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="review">Review</label>
                        <textarea name="review" id="post" class="form-control fr-view" cols="5" rows="5"></textarea>
                    </div>

                    <button type="submit" class="btn btn-primary">Add post</button>
                </div>

                @component('admin.includes.formErrors')
            
                @endcomponent
            </div>
        </div>



    </form>

@endsection
