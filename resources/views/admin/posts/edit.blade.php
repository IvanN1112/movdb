<!DOCTYPE HTML>
@extends('layouts.admin');

@section('content')


    @component('admin.includes.title')
        Edit Post / Authors    
    @endcomponent


    @if (!empty($post))

    <form method="POST" action="/admin/posts/{{ $post->id }}" style="text-align: right">
    @csrf
    @method('DELETE')

    <button type="submit" class="btn btn-info">Delete post</button>
    
    </form>

        <form method="POST" action="/admin/posts/{{$post->id}}" enctype="multipart/form-data">
        @csrf

       @method('PATCH')

        <div class="row">
            <div class="col-sm-4">
                
                <div class="form-group">
                    <label for="file">Movie Pic</label>
                    <div class="">
                        <img src="{{ url('images/posts/' . $post->image['filename']) }}" id="profile-img-tag" width="250px" height="250px" style="object-fit: contain" style="margin-bottom: 0.5rem" alt="">
                    </div>
                    <input type="file" name="file" id="profile-img">
                </div>
            </div>

            <div class="col-sm-8">
                <div class="form-group">
                    
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" value="{{$post->title}}">
                    </div>

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $post->name }}">
                    </div>

                    <div class="form-group">
                        <label for="category_id">Category</label>
                        <select name="category_id" id="category_id" class="form-control">
                            <option disabled selected>select a category</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}" {{ $post->category_id == $category->id ? 'selected' : ''}}>{{ $category->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control desc" cols="5" rows="5">
                            {{$post->description}}
                        </textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="review">Review</label>
                        <textarea name="review" id="post" class="form-control fr-view" cols="5" rows="5">
                            {{ $post->review }}
                        </textarea>
                    </div>

                    <button type="submit" class="btn btn-primary">Edit post</button>
                </div>

                @component('admin.includes.formErrors')
            
                @endcomponent
            </div>
        </div>



    </form>

    @else
        <h3>Sorry, there is no such user...</h3>
    @endif


    

@endsection
