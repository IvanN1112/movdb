@extends('layouts.admin');

@section('content')

<div class="col-sm-11">
    @component('admin.includes.title')
    Edit category  
    @endcomponent




    <div class="row">
        <div class="col-sm-5">
            <form method="POST" action="/admin/categories/{{ $category->id }}">

                @method("PATCH")
                @csrf

                <div class="form-group">
                    <label for="name">Category name</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Enter a new category" value = "{{ ucwords($category->name) }}">
                </div>


                <button type="submit" class="btn btn-primary">Edit</button>
            </form>


            <form method="POST" action="/admin/categories/{{ $category->id }}">
            @csrf
            @method('DELETE')

            <button type="submit" class="btn-xs  btn-danger">Remove category</button>
            
            </form>

            @component('admin.includes.formErrors')
            @endcomponent
        </div>
    </div>

</div>




@endsection
