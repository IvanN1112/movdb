@extends('layouts.admin');

@section('content')

<div class="col-sm-11">
    @component('admin.includes.title')
    Categories List    
    @endcomponent


    @if (Session::has('flash_admin'))
    <div class="flash_message">
        {{ Session('flash_admin') }}
    </div>
    @endif

    <div class="row">
        <div class="col-sm-4">
            <table class="table table-striped admin_users_table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Category name</th>
                    </tr>
                </thead>
                <tbody>
                    @if($categories)
                        @foreach ($categories as $category)
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td>
                                    <a href="{{ url('admin/categories/' . $category->id . '/edit') }}">
                                        {{ $category->name }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif 
                </tbody>
            </table>
        </div>

        <div class="col-sm-5">
            <form method="POST" action="/admin/categories">
                @csrf

                <div class="form-group">
                    <label for="name">Category name</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Enter a new category">
                </div>


                <button type="submit" class="btn btn-primary">Add</button>
            </form>

            @component('admin.includes.formErrors')
            @endcomponent
        </div>
    </div>

</div>




@endsection
