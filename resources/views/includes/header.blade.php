<header>
    <nav class="navbar navbar-expand-lg navbar-dark">
        <a href="/" class="navbar-brand">Mov-Db</a>

        <div class="collapse navbar-collapse">
            <div class="navbar-nav">
                <a href="#" class="nav-item nav-link">Home</a>
                <a href="#" class="nav-item nav-link">Featured</a>
                <a href="#" class="nav-item nav-link">Pricing</a>
            </div>
        </div>

        @if (Auth::user())
        <a href={{url('/admin')}} class="nav-link nav-user-img">
            <i class="fas fa-user"></i>
        </a>
            
        @endif
    </nav>
</header>
