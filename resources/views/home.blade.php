@extends('layouts.app')

@section('content')

@if ($posts)
{{-- slider --}}
<div class="slick-home">
    @foreach ($posts as $post)
        <div>
            <div class="slick-item" style="background:url(/images/posts/{{ $post->image['filename']}}">
                <a href="{{url('/post/'. $post->slug)}}" class="slick-wrapper">
                    <div class="slick-content">
                        <div class="wrapper">
                            <div class="slick-movie">{{$post->name}}</div>
                            <div class="slick-category">{{$post->category['name']}}</div>
                            <div class="slick-title">{{$post->title}}</div>
                        </div>
                    </div>
                </a>
            </div>    
        </div>            
    @endforeach
</div>
@endif

<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-8">
            @if ($postsOther)
            
            <div class="home_post_list">
                <h3>Awesome reviews</h3>

                <div class="row">
                    @foreach ($postsOther as $post)
                        <div class="col-md-6">
                            <div class="item">

                                <div class="date">
                                    Created: {{$post->created_at->diffForHumans()}}
                                </div>

                                <div>
                                    <div class="category">
                                        {{$post->category->name}}
                                    </div>
                                    <div class="name">
                                        {{$post->name}}
                                    </div>
                                    <div class="text_wrapper">
                                        <a href="{{ url('/post/'. $post->slug)}}" class="title">
                                        {{$post->title}}
                                        </a>
                                        <div class="desc">
                                            {{substr($post->description,0,250)}}<a href="{{ url('post/' . $post->slug)}}">
                                            ...read more
                                            </a>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            @endif
        </div>

        <div class="col-md-4">
            <div class="home_top_post_list">
                <h3>Popular reviews</h3>
                @if($topPosts)
                    @foreach ($topPosts as $topPost)
                        <a href="{{ url('post/' . $topPost->slug)}}" class="item">
                            <div class="name">{{$topPost->name}}</div>
                            <div class="name">{{$topPost->title}}</div>
                        </a>
                    @endforeach
                @endif
            </div>
        </div>

    </div>
</div>
@endsection


{{-- jQuery --}}
<script src="{{ asset('vendor/jquery/jquery-3.3.1.min.js')}}"></script>

<script>
    $(document).ready(function () {
        $('.slick-home').slick(
            {
                infinite:true,
                slidesToShow: 4,
                slidesToScroll: 4,
                arrows: false
            }
        );
    });
</script>
