<?php

use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use UniSharp\LaravelFilemanager\Lfm;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Admin, users and posts routes

Route::group(['middleware' => ['active']], function () {
    Route::get('/admin', 'App\Http\Controllers\AdminController@index');
    Route::resources(['/admin/posts' => 'App\Http\Controllers\AdminPostsController']);
    Route::resources(['/admin/categories' => "App\Http\Controllers\AdminCategoriesController"]);
    Route::post('admin/posts/filter', "App\Http\Controllers\AdminPostsController@filter");
});

//Search for posts


Route::group(['middleware' => ['active', 'admin']], function () {
    Route::resources(['/admin/site' => 'App\Http\Controllers\AdminSiteController']);
    Route::resources(['/admin/users' => 'App\Http\Controllers\AdminUsersController']);
});



//Home route

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/post/{id}', "App\Http\Controllers\PostController@show")->name('post');

//File manager routes

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    Lfm::routes();
});













Route::get('/testing', function () {
    return User::find(1)->isActive();
});
